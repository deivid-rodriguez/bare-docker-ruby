FROM buildpack-deps:16.04

# Ruby version to include in the image
ARG RUBY_VERSION=2.7.2

RUN set -ex \
  \
  && buildDeps=' \
    bison \
    dirmngr \
    gnupg2 \
    sudo \
  ' \
  && apt-get update \
  && apt-get install -y --no-install-recommends $buildDeps \
  && rm -rf /var/lib/apt/lists/*

RUN groupadd --system ruby \
  && useradd --system --create-home --no-log-init --uid 1000 --gid ruby ruby

USER ruby

# Install + verify RVM with gpg (https://rvm.io/rvm/security)
RUN set -ex \
  \
  && cd $HOME \
  \
  && gpg2 --quiet --no-tty --logger-fd 1 --keyserver hkp://pool.sks-keyservers.net \
          --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 \
                      7D2BAF1CF37B13E2069D6956105BD0E739499BDB \
  && echo 409B6B1796C275462A1703113804BB82D39DC0E3:6: | \
     gpg2 --quiet --no-tty --logger-fd 1 --import-ownertrust \
  && echo 7D2BAF1CF37B13E2069D6956105BD0E739499BDB:6: | \
     gpg2 --quiet --no-tty --logger-fd 1 --import-ownertrust \
  && curl -sSO https://raw.githubusercontent.com/rvm/rvm/stable/binscripts/rvm-installer \
  && curl -sSO https://raw.githubusercontent.com/rvm/rvm/stable/binscripts/rvm-installer.asc \
  && gpg2 --quiet --no-tty --logger-fd 1 --verify rvm-installer.asc rvm-installer \
  && bash rvm-installer stable \
  && rm rvm-installer rvm-installer.asc

# Workaround tty check, see https://github.com/hashicorp/vagrant/issues/1673#issuecomment-26650102
RUN sed -i 's/^mesg n/tty -s \&\& mesg n/g' $HOME/.profile

# Switch to a bash login shell to allow simple 'rvm' in RUN commands
SHELL ["/bin/bash", "-l", "-c"]

RUN rvm install --autolibs=disable $RUBY_VERSION \
  && rvm use $RUBY_VERSION --default \
  && rvm cleanup all
