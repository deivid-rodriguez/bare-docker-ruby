#!/bin/bash

declare -A RUBY_SHAS

RUBY_SHAS[3.0.1]="d06bccd382d03724b69f674bc46cd6957ba08ed07522694ce44b9e8ffc9c48e2"
RUBY_SHAS[2.7.2]="6e5706d0d4ee4e1e2f883db9d768586b4d06567debea353c796ec45e8321c3d4"
RUBY_SHAS[2.6.6]="364b143def360bac1b74eb56ed60b1a0dca6439b00157ae11ff77d5cd2e92291"

RUBY_MAJOR=${RUBY_VERSION%.*}

if [ -z "$RUBY_VERSION" ]
then
  docker build --file "$DOCKERFILE" --tag "$IMAGE" --no-cache .
else
  if [ "$RUBY_VERSION" = "master" ]
  then
    RUBY_DOWNLOAD_URL=https://cache.ruby-lang.org/pub/ruby/snapshot.tar.xz

    docker build \
      --file "$DOCKERFILE" \
      --tag "$IMAGE" \
      --build-arg "RUBY_DOWNLOAD_URL=$RUBY_DOWNLOAD_URL" \
      --no-cache \
      .
  else
    RUBY_DOWNLOAD_URL="https://cache.ruby-lang.org/pub/ruby/$RUBY_MAJOR/ruby-$RUBY_VERSION.tar.xz"
    RUBY_DOWNLOAD_SHA256=${RUBY_SHAS[$RUBY_MAJOR]}

    docker build \
      --file "$DOCKERFILE" \
      --tag "$IMAGE" \
      --build-arg "RUBY_DOWNLOAD_URL=$RUBY_DOWNLOAD_URL" \
      --build-arg "RUBY_DOWNLOAD_SHA256=$RUBY_DOWNLOAD_SHA256" \
      --no-cache \
      .
  fi
fi
