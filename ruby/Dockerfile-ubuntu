FROM buildpack-deps:16.04

ARG RUBY_DOWNLOAD_URL
ARG RUBY_DOWNLOAD_SHA256

RUN set -ex \
  \
  && buildDeps=' \
    bison \
    dpkg-dev \
    libgdbm-dev \
    libssl-dev \
    sudo \
  ' \
  && apt-get update \
  && apt-get install -y --no-install-recommends $buildDeps \
  && rm -rf /var/lib/apt/lists/*

RUN groupadd --system ruby \
  && useradd --system --create-home --no-log-init --uid 1000 --gid ruby ruby

USER ruby

RUN set -ex \
  \
  && cd $HOME \
  \
  && curl --show-error --location --fail --retry 3 --output ruby.tar.xz "$RUBY_DOWNLOAD_URL" \
  && if [ "$RUBY_DOWNLOAD_SHA256" != "" ]; then echo "$RUBY_DOWNLOAD_SHA256 *ruby.tar.xz" | sha256sum -c -; fi \
  \
  && mkdir -p $HOME/src/ruby \
  && tar -xJf ruby.tar.xz -C $HOME/src/ruby --strip-components=1 \
  && rm ruby.tar.xz \
  \
  && cd $HOME/src/ruby \
  \
  && autoconf \
  && gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
  && ./configure \
    --build="$gnuArch" \
    --disable-install-doc \
    --enable-shared \
    --prefix=$HOME \
  && make -j "$(nproc)" \
  && make install \
  && rm -r $HOME/src/ruby

USER root

RUN set -ex \
  \
  && apt-get purge -y --auto-remove $buildDeps

ENV PATH /home/ruby/bin:$PATH

CMD [ "irb" ]
